import { Router } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  spinner_:boolean = false
  invalid:boolean = false

  credentials = {
    id:''
  }

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login(){
    this.spinner_ = true
    this.authService.login(this.credentials)
      .subscribe(async(data)=>{
        const token = data.token
        window.localStorage.setItem('token', token)
        this.spinner_ = false
        console.log(data)
        this.router.navigate(['prioridades-show'])
      }, error => {
        this.invalid = true
        this.spinner_ = false
      })
  }

}
