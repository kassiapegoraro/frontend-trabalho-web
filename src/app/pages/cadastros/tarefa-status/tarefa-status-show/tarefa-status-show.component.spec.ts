import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaStatusShowComponent } from './tarefa-status-show.component';

describe('TarefaStatusShowComponent', () => {
  let component: TarefaStatusShowComponent;
  let fixture: ComponentFixture<TarefaStatusShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaStatusShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaStatusShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
