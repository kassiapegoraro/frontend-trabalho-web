import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-status-show',
  templateUrl: './tarefa-status-show.component.html',
  styleUrls: ['./tarefa-status-show.component.css']
})
export class TarefaStatusShowComponent implements OnInit {

  status = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getStatus();
  }

  getStatus(){
    this.globalService.getAll('tarefa_status')
    .subscribe((response:any) => {
      this.status = response.data;
      console.log(this.status)
    });
  }

  deleteStatus(status){
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('tarefa_status', status.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getStatus()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', error.error.message, 'danger')
          })
      }
    });
  }
}
