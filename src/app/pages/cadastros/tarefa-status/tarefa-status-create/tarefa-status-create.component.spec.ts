import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaStatusCreateComponent } from './tarefa-status-create.component';

describe('TarefaStatusCreateComponent', () => {
  let component: TarefaStatusCreateComponent;
  let fixture: ComponentFixture<TarefaStatusCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaStatusCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaStatusCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
