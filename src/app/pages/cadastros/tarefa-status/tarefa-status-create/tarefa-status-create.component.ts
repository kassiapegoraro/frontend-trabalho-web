import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-status-create',
  templateUrl: './tarefa-status-create.component.html',
  styleUrls: ['./tarefa-status-create.component.css']
})
export class TarefaStatusCreateComponent implements OnInit {

  status = {
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  postStatus(){
    console.log(this.status)
    this.globalService.post('tarefa_status', this.status)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['tarefa-status-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error, 'danger')
      })
  }

}
