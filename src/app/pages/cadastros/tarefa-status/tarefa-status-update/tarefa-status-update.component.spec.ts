import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaStatusUpdateComponent } from './tarefa-status-update.component';

describe('TarefaStatusUpdateComponent', () => {
  let component: TarefaStatusUpdateComponent;
  let fixture: ComponentFixture<TarefaStatusUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaStatusUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaStatusUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
