import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-status-update',
  templateUrl: './tarefa-status-update.component.html',
  styleUrls: ['./tarefa-status-update.component.css']
})
export class TarefaStatusUpdateComponent implements OnInit {

  id = '';
  status = {
    id: '',
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getStatus()
  }

  getStatus(){
    this.globalService.get('tarefa_status', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.status = response.data[0];
      })
  }

  editStatus(){
    console.log(this.status)
    this.globalService.edit('tarefa_status', this.status.id, this.status)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['tarefa-status-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

}
