import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-grupo-create',
  templateUrl: './grupo-create.component.html',
  styleUrls: ['./grupo-create.component.css']
})
export class GrupoCreateComponent implements OnInit {

  projetos = []
  grupo = {
    id_projeto: null,
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getProjetos()
  }

  getProjetos(){
    this.globalService.getAll('projeto')
      .subscribe((data:any)=>{
        console.log(data)
        this.projetos = data.data
      })
  }

  postGrupo(){
    console.log(this.grupo)
    this.globalService.post('grupo', this.grupo)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['grupo-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

}
