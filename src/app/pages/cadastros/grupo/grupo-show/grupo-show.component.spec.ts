import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupoShowComponent } from './grupo-show.component';

describe('GrupoShowComponent', () => {
  let component: GrupoShowComponent;
  let fixture: ComponentFixture<GrupoShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrupoShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupoShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
