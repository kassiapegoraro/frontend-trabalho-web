import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-grupo-show',
  templateUrl: './grupo-show.component.html',
  styleUrls: ['./grupo-show.component.css']
})
export class GrupoShowComponent implements OnInit {

  
  grupos = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getGrupos();
  }

  getGrupos(){
    this.globalService.getAll('grupo')
    .subscribe((response:any) => {
      this.grupos = response.data;
    });
  }

  deleteGrupo(grupo){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('grupo', grupo.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getGrupos()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', 'O grupo não foi excluido', 'danger')
          })
      }
    });
  }

}
