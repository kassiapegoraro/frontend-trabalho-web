import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-grupo-update',
  templateUrl: './grupo-update.component.html',
  styleUrls: ['./grupo-update.component.css']
})
export class GrupoUpdateComponent implements OnInit {

  id = '';

  projetos = []
  grupo = {
    id: '',
    id_projeto: null,
    descricao: ''
  }
  constructor(
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.getProjetos();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getGrupo()
  }

  getGrupo(){
    this.globalService.get('grupo', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.grupo = response.data[0];
      })
  }

  getProjetos(){
    this.globalService.getAll('projeto')
      .subscribe((data:any)=>{
        console.log(data)
        this.projetos = data.data
      })
  }

  editGrupo(){
    console.log(this.grupo)
    this.globalService.edit('grupo', this.grupo.id, this.grupo)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['grupo-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }
}
