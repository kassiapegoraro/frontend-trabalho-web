import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-sistema-create',
  templateUrl: './sistema-create.component.html',
  styleUrls: ['./sistema-create.component.css']
})
export class SistemaCreateComponent implements OnInit {

  sistema = {
    nome: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  postSistema(){
    console.log(this.sistema)
    this.globalService.post('sistema', this.sistema)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['sistemas-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error, 'danger')
      })
  }

}
