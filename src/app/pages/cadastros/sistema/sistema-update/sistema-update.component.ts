import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-sistema-update',
  templateUrl: './sistema-update.component.html',
  styleUrls: ['./sistema-update.component.css']
})
export class SistemaUpdateComponent implements OnInit {

  id = '';
  sistema = {
    id_sistema: '',
    nome: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getSistema()
  }

  getSistema(){
    this.globalService.get('sistema', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.sistema = response.data[0];
      })
  }

  editSistema(){
    console.log(this.sistema)
    this.globalService.edit('sistema', this.sistema.id_sistema, this.sistema)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['sistemas-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

}
