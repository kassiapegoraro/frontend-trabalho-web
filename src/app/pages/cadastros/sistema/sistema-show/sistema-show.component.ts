import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-sistema-show',
  templateUrl: './sistema-show.component.html',
  styleUrls: ['./sistema-show.component.css']
})
export class SistemaShowComponent implements OnInit {

  sistemas = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getSistemas();
  }

  getSistemas(){
    this.globalService.getAll('sistema')
    .subscribe((response:any) => {
      this.sistemas = response.data;
    });
  }

  deleteSistema(sistema){
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('sistema', sistema.id_sistema)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getSistemas()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', error.error.message, 'danger')
          })
      }
    });
  }

}
