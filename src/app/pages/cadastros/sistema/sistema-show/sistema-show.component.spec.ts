import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SistemaShowComponent } from './sistema-show.component';

describe('SistemaShowComponent', () => {
  let component: SistemaShowComponent;
  let fixture: ComponentFixture<SistemaShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SistemaShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SistemaShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
