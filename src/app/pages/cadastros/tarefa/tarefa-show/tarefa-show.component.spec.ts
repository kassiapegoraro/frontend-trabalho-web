import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaShowComponent } from './tarefa-show.component';

describe('TarefaShowComponent', () => {
  let component: TarefaShowComponent;
  let fixture: ComponentFixture<TarefaShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
