import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-show',
  templateUrl: './tarefa-show.component.html',
  styleUrls: ['./tarefa-show.component.css']
})
export class TarefaShowComponent implements OnInit {

  tarefas = [];

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getTarefas();
  }

  getTarefas(){
    this.globalService.getAll('tarefa')
    .subscribe((response:any) => {
      this.tarefas = response.data;
      console.log(this.tarefas)
    });
  }

  deleteTarefa(tarefa){
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('tarefa', tarefa.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getTarefas()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', error.error.message, 'danger')
          })
      }
    });
  }

}
