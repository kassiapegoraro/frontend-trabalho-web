import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-update',
  templateUrl: './tarefa-update.component.html',
  styleUrls: ['./tarefa-update.component.css']
})
export class TarefaUpdateComponent implements OnInit {

  id = '';
  tarefa = {
    id: '',
    titulo: '',
    descricao: '',
    id_projeto:null,
    id_dev: null,
    tempo_estimado: '' ,
    data_inicio: '',
    data_fim: '',
    // id_pai_tarefa: null,
    id_tipo_tarefa: null ,
    id_status_tarefa: null ,
    data_inicio_dev: '' ,
    data_fim_dev: '' ,
    tempo_realizado: '' ,
    authorized: '' ,
    id_prioridade: null ,
    complexidade: '',
    impacto: '',
    id_grupo: null
  }

  projetos = [];

  usuarios = [];

  tarefas = [];

  tipos = [];

  status = [];

  prioridades = [];

  grupos = [];

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.getProjetos();
    this.getUsuarios();
    this.getTarefas();
    this.getTipoTarefa();
    this.getStatus();
    this.getPrioridades();
    this.getGrupos();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getTarefa()
  }

  getTarefa(){
    this.globalService.get('tarefa', this.id)
      .subscribe((response: any)=>{
        this.tarefa.id = response.data[0].id;
        this.tarefa.titulo = response.data[0].titulo;
        this.tarefa.descricao = response.data[0].descricao;
        this.tarefa.id_projeto = response.data[0].id_projeto;
        this.tarefa.tempo_estimado = response.data[0].tempo_estimado ;
        this.tarefa.data_inicio = response.data[0].data_inicio;
        this.tarefa.data_fim = response.data[0].data_fim;
        this.tarefa.id_tipo_tarefa = response.data[0].id_tipo_tarefa ;
        this.tarefa.id_status_tarefa = response.data[0].id_status_tarefa ;
        this.tarefa.data_inicio_dev = response.data[0].data_inicio_dev ;
        this.tarefa.data_fim_dev = response.data[0].data_fim_dev ;
        this.tarefa.tempo_realizado = response.data[0].tempo_realizado ;
        this.tarefa.authorized = response.data[0].authorized ;
        this.tarefa.id_prioridade = response.data[0].id_prioridade ;
        this.tarefa.complexidade = response.data[0].complexidade;
        this.tarefa.impacto = response.data[0].impacto;
        this.tarefa.id_grupo = response.data[0].id_grupo;
        // this.tarefa.id_pai_tarefa = response.data[0].id_pai_tarefa;
        this.tarefa.id_dev = response.data[0].id_dev;
        
      })
  }

  editTarefa(){
    console.log(this.tarefa)
    this.globalService.edit('tarefa', this.tarefa.id, this.tarefa)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['tarefa-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

  getTipoTarefa(){
    this.globalService.getAll('tarefa_tipo')
      .subscribe((tarefa_tipo:any)=>{
        this.tipos = tarefa_tipo.data
      })
  }

  getPrioridades(){
    this.globalService.getAll('prioridade')
      .subscribe((prioridade:any)=>{
        this.prioridades = prioridade.data
      })
  }

  getGrupos(){
    this.globalService.getAll('grupo')
      .subscribe((grupo:any)=>{
        this.grupos = grupo.data
      })
  }

  getStatus(){
    this.globalService.getAll('tarefa_status')
      .subscribe((tarefa_status:any)=>{
        this.status = tarefa_status.data
      })
  }

  getProjetos(){
    this.globalService.getAll('projeto')
      .subscribe((projetos:any)=>{
        this.projetos = projetos.data
      })
  }

  getTarefas(){
    this.globalService.getAll('tarefa')
      .subscribe((tarefas:any)=>{
        this.tarefas = tarefas.data
      })
  }

  getUsuarios(){
    this.globalService.getAll('usuario')
      .subscribe((users:any)=>{
        this.usuarios = users.data
      })
  }
}
