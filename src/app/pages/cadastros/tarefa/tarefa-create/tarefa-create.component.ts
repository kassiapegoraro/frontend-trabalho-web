import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-create',
  templateUrl: './tarefa-create.component.html',
  styleUrls: ['./tarefa-create.component.css']
})
export class TarefaCreateComponent implements OnInit {

  tarefa = {
    titulo: '',
    descricao: '',
    id_projeto:null,
    id_criador:null,
    id_dev: null,
    tempo_estimado: '' ,
    data_inicio: '',
    data_fim: '',
    id_pai_tarefa: null,
    id_tipo_tarefa: null ,
    id_status_tarefa: null ,
    data_inicio_dev: '' ,
    data_fim_dev: '' ,
    tempo_realizado: '' ,
    authorized: '' ,
    id_prioridade: null ,
    complexidade: '',
    impacto: '',
    id_grupo: null
  }

  projetos = [];

  usuarios = [];

  tarefas = [];

  tipos = [];

  status = [];

  prioridades = [];

  grupos = [];

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getProjetos();
    this.getUsuarios();
    this.getTarefas();
    this.getTipoTarefa();
    this.getStatus();
    this.getPrioridades();
    this.getGrupos();
  }

  getTipoTarefa(){
    this.globalService.getAll('tarefa_tipo')
      .subscribe((tarefa_tipo:any)=>{
        console.log(tarefa_tipo)
        this.tipos = tarefa_tipo.data
      })
  }

  getPrioridades(){
    this.globalService.getAll('prioridade')
      .subscribe((prioridade:any)=>{
        console.log(prioridade)
        this.prioridades = prioridade.data
      })
  }

  getGrupos(){
    this.globalService.getAll('grupo')
      .subscribe((grupo:any)=>{
        console.log(grupo)
        this.grupos = grupo.data
      })
  }

  getStatus(){
    this.globalService.getAll('tarefa_status')
      .subscribe((tarefa_status:any)=>{
        console.log(tarefa_status)
        this.status = tarefa_status.data
      })
  }

  getProjetos(){
    this.globalService.getAll('projeto')
      .subscribe((projetos:any)=>{
        console.log(projetos)
        this.projetos = projetos.data
      })
  }

  getTarefas(){
    this.globalService.getAll('tarefa')
      .subscribe((tarefas:any)=>{
        console.log(tarefas)
        this.tarefas = tarefas.data
      })
  }

  getUsuarios(){
    this.globalService.getAll('usuario')
      .subscribe((users:any)=>{
        console.log(users)
        this.usuarios = users.data
      })
  }

  postTarefa(){
    console.log(this.tarefa)
    this.globalService.post('tarefa', this.tarefa)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['tarefa-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error, 'danger')
      })
  }
}
