import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-projeto-show',
  templateUrl: './projeto-show.component.html',
  styleUrls: ['./projeto-show.component.css']
})
export class ProjetoShowComponent implements OnInit {

  projetos = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getProjetos();
  }

  getProjetos(){
    this.globalService.getAll('projeto')
    .subscribe((response:any) => {
      this.projetos = response.data;
    });
  }

  deleteProjeto(projeto){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('projeto', projeto.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getProjetos()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', 'O projeto não foi excluído', 'danger')
          })
        
      }
    });
  }
}
