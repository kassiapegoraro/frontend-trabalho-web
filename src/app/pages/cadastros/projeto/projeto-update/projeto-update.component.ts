import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-projeto-update',
  templateUrl: './projeto-update.component.html',
  styleUrls: ['./projeto-update.component.css']
})
export class ProjetoUpdateComponent implements OnInit {

  id = '';

  sistemas = [];

  projeto = {
    id: '',
    titulo: null,
    descricao: '',
    data_inicio: null,
    data_fim: null,
    id_criador: 2,
    id_sistema: null
  }
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getSistemas()
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getProjeto()
  }

  getProjeto(){
    this.globalService.get('projeto', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.projeto = response.data[0];
      })
  }

  getSistemas(){
    this.globalService.getAll('sistema')
      .subscribe((data:any)=>{
        console.log(data)
        this.sistemas = data.data
      })
  }

  editProjeto(){
    console.log(this.projeto)
    this.globalService.edit('projeto', this.projeto.id, this.projeto)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['projeto-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }
}
