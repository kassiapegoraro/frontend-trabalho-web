import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetoUpdateComponent } from './projeto-update.component';

describe('ProjetoUpdateComponent', () => {
  let component: ProjetoUpdateComponent;
  let fixture: ComponentFixture<ProjetoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjetoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
