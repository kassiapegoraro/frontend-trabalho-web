import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-projeto-create',
  templateUrl: './projeto-create.component.html',
  styleUrls: ['./projeto-create.component.css']
})
export class ProjetoCreateComponent implements OnInit {

  sistemas = []
  projeto = {
    titulo: null,
    descricao: '',
    data_inicio: null,
    data_fim: null,
    id_criador: 2,
    id_sistema: null
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getSistemas()
  }

  getSistemas(){
    this.globalService.getAll('sistema')
      .subscribe((data:any)=>{
        console.log(data)
        this.sistemas = data.data
      })
  }

  postProjeto(){
    console.log(this.projeto)
    this.globalService.post('projeto', this.projeto)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['projeto-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', 'Projeto não foi criado', 'danger')
      })
  }

}
