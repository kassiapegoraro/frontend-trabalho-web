import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrioridadesUpdateComponent } from './prioridades-update.component';

describe('PrioridadesUpdateComponent', () => {
  let component: PrioridadesUpdateComponent;
  let fixture: ComponentFixture<PrioridadesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrioridadesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrioridadesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
