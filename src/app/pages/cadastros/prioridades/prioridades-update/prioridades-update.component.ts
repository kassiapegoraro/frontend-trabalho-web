import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-prioridades-update',
  templateUrl: './prioridades-update.component.html',
  styleUrls: ['./prioridades-update.component.css']
})
export class PrioridadesUpdateComponent implements OnInit {

  id = '';
  prioridade = {
    id: '',
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getPrioridade()
  }

  getPrioridade(){
    this.globalService.get('prioridade', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.prioridade = response.data[0];
      })
  }

  editPrioridade(){
    console.log(this.prioridade)
    this.globalService.edit('prioridade', this.prioridade.id, this.prioridade)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['prioridades-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

}
