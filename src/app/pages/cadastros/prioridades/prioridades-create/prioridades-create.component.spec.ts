import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrioridadesCreateComponent } from './prioridades-create.component';

describe('PrioridadesCreateComponent', () => {
  let component: PrioridadesCreateComponent;
  let fixture: ComponentFixture<PrioridadesCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrioridadesCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrioridadesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
