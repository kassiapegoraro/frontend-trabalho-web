import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { GlobalService } from './../../../../services/global.service';
import { NotificationComponent } from 'app/components/notification/notification.component';

@Component({
  selector: 'app-prioridades-create',
  templateUrl: './prioridades-create.component.html',
  styleUrls: ['./prioridades-create.component.css']
})
export class PrioridadesCreateComponent implements OnInit {

  prioridade = {
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  postPrioridade(){
    console.log(this.prioridade)
    this.globalService.post('prioridade', this.prioridade)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['prioridades-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

}
