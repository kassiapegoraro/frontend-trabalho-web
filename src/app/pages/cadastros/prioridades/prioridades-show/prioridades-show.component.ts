import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from './../../../../services/global.service';

export interface Prioridades {
  id: number;
  descricao: string;
}

@Component({
  selector: 'app-prioridades-show',
  templateUrl: './prioridades-show.component.html',
  styleUrls: ['./prioridades-show.component.css']
})
export class PrioridadesShowComponent implements OnInit {

  prioridades = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getPrioridades();
  }

  getPrioridades(){
    this.globalService.getAll('prioridade')
    .subscribe((response:any) => {
      this.prioridades = response.data;
    });
  }

  deletePrioridade(prioridade){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('prioridade', prioridade.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getPrioridades()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', error.error.message, 'danger')
          })
        
      }
    });


  }

}
