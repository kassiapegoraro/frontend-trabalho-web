import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrioridadesShowComponent } from './prioridades-show.component';

describe('PrioridadesShowComponent', () => {
  let component: PrioridadesShowComponent;
  let fixture: ComponentFixture<PrioridadesShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrioridadesShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrioridadesShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
