import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-comentario-create',
  templateUrl: './comentario-create.component.html',
  styleUrls: ['./comentario-create.component.css']
})
export class ComentarioCreateComponent implements OnInit {

  usuarios = []
  tarefas = []
  comentarios = []
  comentario = {
    id_usuario: '',
    id_tarefa: '',
    descricao: '',
    id_pai_comentario: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getUsuarios()
    this.getTarefas()
    this.getComentarios()
  }

  getUsuarios(){
    this.globalService.getAll('usuario')
      .subscribe((users:any)=>{
        console.log(users)
        this.usuarios = users.data
      })
  }

  getComentarios(){
    this.globalService.getAll('comentario')
      .subscribe((c:any)=>{
        console.log(c)
        this.comentarios = c.data
      })
  }

  getTarefas(){
    this.globalService.getAll('tarefa')
      .subscribe((data:any)=>{
        console.log(data)
        this.tarefas = data.data
      })
  }

  postComentario(){
    console.log(this.comentario)
    this.globalService.post('comentario', this.comentario)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['comentario-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }
}
