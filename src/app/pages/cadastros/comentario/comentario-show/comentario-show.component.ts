import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-comentario-show',
  templateUrl: './comentario-show.component.html',
  styleUrls: ['./comentario-show.component.css']
})
export class ComentarioShowComponent implements OnInit {

  
  comentarios = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getComentarios();
  }

  getComentarios(){
    this.globalService.getAll('comentario')
    .subscribe((response:any) => {
      this.comentarios = response.data;
    });
  }

  deleteComentario(comentario){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('comentario', comentario.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getComentarios()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', error.error.message, 'danger')
          })
        
      }
    });


  }


}
