import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentarioShowComponent } from './comentario-show.component';

describe('ComentarioShowComponent', () => {
  let component: ComentarioShowComponent;
  let fixture: ComponentFixture<ComentarioShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComentarioShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentarioShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
