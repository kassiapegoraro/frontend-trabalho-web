import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-comentario-update',
  templateUrl: './comentario-update.component.html',
  styleUrls: ['./comentario-update.component.css']
})
export class ComentarioUpdateComponent implements OnInit {

  id = ''
  usuarios = []
  tarefas = []
  comentarios = []

  comentario = {
    id: '',
    id_usuario: '',
    id_tarefa: '',
    descricao: '',
    id_pai_comentario: ''
  }

  constructor(
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getUsuarios();
    this.getTarefas();
    this.getComentarios();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getComentario()
  }

  getUsuarios(){
    this.globalService.getAll('usuario')
      .subscribe((users:any)=>{
        console.log(users)
        this.usuarios = users.data
      })
  }

  getComentarios(){
    this.globalService.getAll('comentario')
      .subscribe((c:any)=>{
        console.log(c)
        this.comentarios = c.data
      })
  }

  getTarefas(){
    this.globalService.getAll('tarefa')
      .subscribe((data:any)=>{
        console.log(data)
        this.tarefas = data.data
      })
  }

  getComentario(){
    this.globalService.get('comentario', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.comentario = response.data[0];
      })
  }

  editComentario(){
    console.log(this.comentario)
    this.globalService.edit('comentario', this.comentario.id, this.comentario)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['comentario-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }

}
