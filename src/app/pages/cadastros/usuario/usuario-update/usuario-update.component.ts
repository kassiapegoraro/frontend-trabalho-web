import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-usuario-update',
  templateUrl: './usuario-update.component.html',
  styleUrls: ['./usuario-update.component.css']
})
export class UsuarioUpdateComponent implements OnInit {

 
  id = '';
  usuario = {
    id_usuario: '',
    nome: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getUsuario()
  }

  getUsuario(){
    this.globalService.get('usuario', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.usuario = response.data[0];
      })
  }

  editUsuario(){
    console.log(this.usuario)
    this.globalService.edit('usuario', this.usuario.id_usuario, this.usuario)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['usuario-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }


}
