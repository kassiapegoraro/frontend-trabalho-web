import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-usuario-create',
  templateUrl: './usuario-create.component.html',
  styleUrls: ['./usuario-create.component.css']
})
export class UsuarioCreateComponent implements OnInit {

  
  usuario = {
    nome: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  postUsuario(){
    console.log(this.usuario)
    this.globalService.post('usuario', this.usuario)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['usuario-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error, 'danger')
      })
  }


}
