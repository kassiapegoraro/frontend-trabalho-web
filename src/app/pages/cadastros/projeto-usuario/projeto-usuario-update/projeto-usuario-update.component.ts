import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-projeto-usuario-update',
  templateUrl: './projeto-usuario-update.component.html',
  styleUrls: ['./projeto-usuario-update.component.css']
})
export class ProjetoUsuarioUpdateComponent implements OnInit {

  id = '';
  projetos = []
  usuarios = []

  projeto_usuario = {
    id: null,
    id_usuario: '',
    id_projeto: '',

  }
  constructor(
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getProjetos()
    this.getUsuarios()
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getProjetoUsuario()
  }

  getProjetoUsuario(){
    this.globalService.get('projeto_usuario', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.projeto_usuario.id = response.data[0].id;
        this.projeto_usuario.id_projeto = response.data[0].id_projeto;
        this.projeto_usuario.id_usuario = response.data[0].id_usuario;
      })
  }

  getProjetos(){
    this.globalService.getAll('projeto')
      .subscribe((data:any)=>{
        console.log(data)
        this.projetos = data.data
      })
  }

  getUsuarios(){
    this.globalService.getAll('usuario')
      .subscribe((data:any)=>{
        console.log(data)
        this.usuarios = data.data
      })
  }

  editProjetoUsuario(){
    console.log(this.projeto_usuario)
    this.globalService.edit('projeto_usuario', this.projeto_usuario.id, this.projeto_usuario)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['projeto-usuario-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }
}
