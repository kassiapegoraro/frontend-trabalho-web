import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetoUsuarioUpdateComponent } from './projeto-usuario-update.component';

describe('ProjetoUsuarioUpdateComponent', () => {
  let component: ProjetoUsuarioUpdateComponent;
  let fixture: ComponentFixture<ProjetoUsuarioUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjetoUsuarioUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetoUsuarioUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
