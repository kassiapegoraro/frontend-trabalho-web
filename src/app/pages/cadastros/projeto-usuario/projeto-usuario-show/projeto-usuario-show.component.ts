import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-projeto-usuario-show',
  templateUrl: './projeto-usuario-show.component.html',
  styleUrls: ['./projeto-usuario-show.component.css']
})
export class ProjetoUsuarioShowComponent implements OnInit {

  projeto_usu = []

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getProjetos();
  }

  getProjetos(){
    this.globalService.getAll('projeto_usuario')
    .subscribe((response:any) => {
      this.projeto_usu = response.data;
      console.log(this.projeto_usu)
    });
  }

  deleteProjeto(projeto){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('projeto_usuario', projeto.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getProjetos()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', 'O projeto-usuario não foi excluído', 'danger')
          })
        
      }
    });
  }
}
