import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetoUsuarioShowComponent } from './projeto-usuario-show.component';

describe('ProjetoUsuarioShowComponent', () => {
  let component: ProjetoUsuarioShowComponent;
  let fixture: ComponentFixture<ProjetoUsuarioShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjetoUsuarioShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetoUsuarioShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
