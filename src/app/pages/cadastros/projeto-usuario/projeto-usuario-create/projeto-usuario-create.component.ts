import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-projeto-usuario-create',
  templateUrl: './projeto-usuario-create.component.html',
  styleUrls: ['./projeto-usuario-create.component.css']
})
export class ProjetoUsuarioCreateComponent implements OnInit {

  projetos = []
  usuarios = []
  
  projeto_usuario = {
    id_usuario: '',
    id_projeto: '',

  }
  constructor(
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getProjetos()
    this.getUsuarios()
  }

  getProjetos(){
    this.globalService.getAll('projeto')
      .subscribe((data:any)=>{
        console.log(data)
        this.projetos = data.data
      })
  }

  getUsuarios(){
    this.globalService.getAll('usuario')
      .subscribe((data:any)=>{
        console.log(data)
        this.usuarios = data.data
      })
  }

  postProjetoUsuario(){
    console.log(this.projeto_usuario)
    this.globalService.post('projeto_usuario', this.projeto_usuario)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['projeto-usuario-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', 'Projeto/Usuario não foi criado', 'danger')
      })
  }
}
