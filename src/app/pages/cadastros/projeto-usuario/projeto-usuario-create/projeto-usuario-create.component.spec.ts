import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetoUsuarioCreateComponent } from './projeto-usuario-create.component';

describe('ProjetoUsuarioCreateComponent', () => {
  let component: ProjetoUsuarioCreateComponent;
  let fixture: ComponentFixture<ProjetoUsuarioCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjetoUsuarioCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetoUsuarioCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
