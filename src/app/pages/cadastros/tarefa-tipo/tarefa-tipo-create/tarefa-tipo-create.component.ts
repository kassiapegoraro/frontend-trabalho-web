import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-tipo-create',
  templateUrl: './tarefa-tipo-create.component.html',
  styleUrls: ['./tarefa-tipo-create.component.css']
})
export class TarefaTipoCreateComponent implements OnInit {

 
  tipo = {
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  postTipo(){
    console.log(this.tipo)
    this.globalService.post('tarefa_tipo', this.tipo)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['tarefa-tipo-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error, 'danger')
      })
  }

}
