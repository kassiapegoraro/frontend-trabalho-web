import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaTipoCreateComponent } from './tarefa-tipo-create.component';

describe('TarefaTipoCreateComponent', () => {
  let component: TarefaTipoCreateComponent;
  let fixture: ComponentFixture<TarefaTipoCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaTipoCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaTipoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
