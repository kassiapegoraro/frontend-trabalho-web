import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaTipoShowComponent } from './tarefa-tipo-show.component';

describe('TarefaTipoShowComponent', () => {
  let component: TarefaTipoShowComponent;
  let fixture: ComponentFixture<TarefaTipoShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaTipoShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaTipoShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
