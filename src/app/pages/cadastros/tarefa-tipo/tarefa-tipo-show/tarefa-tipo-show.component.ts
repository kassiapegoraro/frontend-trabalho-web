import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-tipo-show',
  templateUrl: './tarefa-tipo-show.component.html',
  styleUrls: ['./tarefa-tipo-show.component.css']
})
export class TarefaTipoShowComponent implements OnInit {

  tipos = [];
  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getTipos();
  }

  getTipos(){
    this.globalService.getAll('tarefa_tipo')
    .subscribe((response:any) => {
      this.tipos = response.data;
      console.log(this.tipos)
    });
  }

  deleteTipo(tipo){
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirmação',
        message: 'Você tem certeza que deseja excluir?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.globalService.delete('tarefa_tipo', tipo.id)
          .subscribe((response:any)=>{
            this.notification.showNotification('top', 'center', response.message, 'success')
            this.getTipos()
          }, error =>{
            console.log(error)
            this.notification.showNotification('top', 'center', error.error.message, 'danger')
          })
      }
    });
  }
}
