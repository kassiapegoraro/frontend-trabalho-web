import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationComponent } from 'app/components/notification/notification.component';
import { GlobalService } from 'app/services/global.service';

@Component({
  selector: 'app-tarefa-tipo-update',
  templateUrl: './tarefa-tipo-update.component.html',
  styleUrls: ['./tarefa-tipo-update.component.css']
})
export class TarefaTipoUpdateComponent implements OnInit {
 
  id = '';
  tipo = {
    id: '',
    descricao: ''
  }

  constructor( 
    private globalService: GlobalService,
    private notification: NotificationComponent,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getTipo()
  }

  getTipo(){
    this.globalService.get('tarefa_tipo', this.id)
      .subscribe((response: any)=>{
        console.log(response)
        this.tipo = response.data[0];
      })
  }

  editTipo(){
    console.log(this.tipo)
    this.globalService.edit('tarefa_tipo', this.tipo.id, this.tipo)
      .subscribe((response:any)=>{
        console.log(response)
        this.notification.showNotification('top', 'center', response.message, 'success')
        this.router.navigate(['tarefa-tipo-show'])
      }, error => {
        console.log(error)
        this.notification.showNotification('top', 'center', error.error.message, 'danger')
      })
  }


}
