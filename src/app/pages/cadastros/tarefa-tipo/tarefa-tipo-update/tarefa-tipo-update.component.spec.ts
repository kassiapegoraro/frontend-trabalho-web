import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaTipoUpdateComponent } from './tarefa-tipo-update.component';

describe('TarefaTipoUpdateComponent', () => {
  let component: TarefaTipoUpdateComponent;
  let fixture: ComponentFixture<TarefaTipoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaTipoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaTipoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
