import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { PrioridadesShowComponent } from './prioridades/prioridades-show/prioridades-show.component';
import { PrioridadesUpdateComponent } from './prioridades/prioridades-update/prioridades-update.component';
import { PrioridadesCreateComponent } from './prioridades/prioridades-create/prioridades-create.component';
import { AppRoutingModule } from 'app/app.routing';
import { ComponentsModule } from 'app/components/components.module';
import { SistemaCreateComponent } from './sistema/sistema-create/sistema-create.component';
import { SistemaUpdateComponent } from './sistema/sistema-update/sistema-update.component';
import { SistemaShowComponent } from './sistema/sistema-show/sistema-show.component';
import { TarefaStatusShowComponent } from './tarefa-status/tarefa-status-show/tarefa-status-show.component';
import { TarefaStatusUpdateComponent } from './tarefa-status/tarefa-status-update/tarefa-status-update.component';
import { TarefaStatusCreateComponent } from './tarefa-status/tarefa-status-create/tarefa-status-create.component';
import { TarefaTipoCreateComponent } from './tarefa-tipo/tarefa-tipo-create/tarefa-tipo-create.component';
import { TarefaTipoShowComponent } from './tarefa-tipo/tarefa-tipo-show/tarefa-tipo-show.component';
import { TarefaTipoUpdateComponent } from './tarefa-tipo/tarefa-tipo-update/tarefa-tipo-update.component';
import { UsuarioShowComponent } from './usuario/usuario-show/usuario-show.component';
import { UsuarioUpdateComponent } from './usuario/usuario-update/usuario-update.component';
import { UsuarioCreateComponent } from './usuario/usuario-create/usuario-create.component';
import { TarefaCreateComponent } from './tarefa/tarefa-create/tarefa-create.component';
import { TarefaShowComponent } from './tarefa/tarefa-show/tarefa-show.component';
import { TarefaUpdateComponent } from './tarefa/tarefa-update/tarefa-update.component';
import { ComentarioUpdateComponent } from './comentario/comentario-update/comentario-update.component';
import { ComentarioShowComponent } from './comentario/comentario-show/comentario-show.component';
import { ComentarioCreateComponent } from './comentario/comentario-create/comentario-create.component';
import { GrupoShowComponent } from './grupo/grupo-show/grupo-show.component';
import { GrupoCreateComponent } from './grupo/grupo-create/grupo-create.component';
import { GrupoUpdateComponent } from './grupo/grupo-update/grupo-update.component';
import { ProjetoUpdateComponent } from './projeto/projeto-update/projeto-update.component';
import { ProjetoShowComponent } from './projeto/projeto-show/projeto-show.component';
import { ProjetoCreateComponent } from './projeto/projeto-create/projeto-create.component';
import { ProjetoUsuarioCreateComponent } from './projeto-usuario/projeto-usuario-create/projeto-usuario-create.component';
import { ProjetoUsuarioUpdateComponent } from './projeto-usuario/projeto-usuario-update/projeto-usuario-update.component';
import { ProjetoUsuarioShowComponent } from './projeto-usuario/projeto-usuario-show/projeto-usuario-show.component';

@NgModule({
  declarations: [
    PrioridadesShowComponent, 
    PrioridadesUpdateComponent, 
    PrioridadesCreateComponent, 
    SistemaCreateComponent, 
    SistemaUpdateComponent, 
    SistemaShowComponent, 
    TarefaStatusShowComponent, 
    TarefaStatusUpdateComponent, 
    TarefaStatusCreateComponent,
    TarefaTipoShowComponent, 
    TarefaTipoUpdateComponent, 
    TarefaTipoCreateComponent, 
    UsuarioShowComponent, 
    UsuarioUpdateComponent, 
    UsuarioCreateComponent, 
    TarefaCreateComponent, 
    TarefaShowComponent, 
    TarefaUpdateComponent, 
    ComentarioUpdateComponent, 
    ComentarioShowComponent, 
    ComentarioCreateComponent, 
    GrupoShowComponent, 
    GrupoCreateComponent, 
    GrupoUpdateComponent, ProjetoUpdateComponent, ProjetoShowComponent, ProjetoCreateComponent, ProjetoUsuarioCreateComponent, ProjetoUsuarioUpdateComponent, ProjetoUsuarioShowComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    MatFormFieldModule,
    RouterModule,
    AppRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule
  ]
})
export class CadastrosModule { }
