import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { AuthService } from './services/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
 
    constructor(
      private authService: AuthService
    ) { }

    intercept(
      request: HttpRequest<any>,
      next: HttpHandler, ): Observable<HttpEvent<any>> {
      const dupReq = request.clone({
        headers: request.headers.set('Authorization', `bearer ${this.authService.getToken()}`)
      });
      return next.handle(dupReq);
    }
}