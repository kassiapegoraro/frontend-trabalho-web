import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { PrioridadesCreateComponent } from 'app/pages/cadastros/prioridades/prioridades-create/prioridades-create.component';
import { PrioridadesShowComponent } from 'app/pages/cadastros/prioridades/prioridades-show/prioridades-show.component';
import { PrioridadesUpdateComponent } from 'app/pages/cadastros/prioridades/prioridades-update/prioridades-update.component';
import { SistemaCreateComponent } from 'app/pages/cadastros/sistema/sistema-create/sistema-create.component';
import { SistemaShowComponent } from 'app/pages/cadastros/sistema/sistema-show/sistema-show.component';
import { SistemaUpdateComponent } from 'app/pages/cadastros/sistema/sistema-update/sistema-update.component';
import { TarefaStatusCreateComponent } from 'app/pages/cadastros/tarefa-status/tarefa-status-create/tarefa-status-create.component';
import { TarefaStatusShowComponent } from 'app/pages/cadastros/tarefa-status/tarefa-status-show/tarefa-status-show.component';
import { TarefaStatusUpdateComponent } from 'app/pages/cadastros/tarefa-status/tarefa-status-update/tarefa-status-update.component';
import { TarefaTipoCreateComponent } from 'app/pages/cadastros/tarefa-tipo/tarefa-tipo-create/tarefa-tipo-create.component';
import { TarefaTipoShowComponent } from 'app/pages/cadastros/tarefa-tipo/tarefa-tipo-show/tarefa-tipo-show.component';
import { TarefaTipoUpdateComponent } from 'app/pages/cadastros/tarefa-tipo/tarefa-tipo-update/tarefa-tipo-update.component';
import { UsuarioCreateComponent } from 'app/pages/cadastros/usuario/usuario-create/usuario-create.component';
import { UsuarioShowComponent } from 'app/pages/cadastros/usuario/usuario-show/usuario-show.component';
import { UsuarioUpdateComponent } from 'app/pages/cadastros/usuario/usuario-update/usuario-update.component';
import { TarefaCreateComponent } from 'app/pages/cadastros/tarefa/tarefa-create/tarefa-create.component';
import { TarefaShowComponent } from 'app/pages/cadastros/tarefa/tarefa-show/tarefa-show.component';
import { TarefaUpdateComponent } from 'app/pages/cadastros/tarefa/tarefa-update/tarefa-update.component';
import { ComentarioShowComponent } from 'app/pages/cadastros/comentario/comentario-show/comentario-show.component';
import { ComentarioCreateComponent } from 'app/pages/cadastros/comentario/comentario-create/comentario-create.component';
import { ComentarioUpdateComponent } from 'app/pages/cadastros/comentario/comentario-update/comentario-update.component';
import { GrupoShowComponent } from 'app/pages/cadastros/grupo/grupo-show/grupo-show.component';
import { GrupoCreateComponent } from 'app/pages/cadastros/grupo/grupo-create/grupo-create.component';
import { GrupoUpdateComponent } from 'app/pages/cadastros/grupo/grupo-update/grupo-update.component';
import { ProjetoCreateComponent } from 'app/pages/cadastros/projeto/projeto-create/projeto-create.component';
import { ProjetoShowComponent } from 'app/pages/cadastros/projeto/projeto-show/projeto-show.component';
import { ProjetoUpdateComponent } from 'app/pages/cadastros/projeto/projeto-update/projeto-update.component';
import { ProjetoUsuarioCreateComponent } from 'app/pages/cadastros/projeto-usuario/projeto-usuario-create/projeto-usuario-create.component';
import { ProjetoUsuarioShowComponent } from 'app/pages/cadastros/projeto-usuario/projeto-usuario-show/projeto-usuario-show.component';
import { ProjetoUsuarioUpdateComponent } from 'app/pages/cadastros/projeto-usuario/projeto-usuario-update/projeto-usuario-update.component';
import { LoginComponent } from 'app/pages/auth/login/login.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'prioridades-show',             component: PrioridadesShowComponent },
    { path: 'prioridades-update/:id',       component: PrioridadesUpdateComponent },
    { path: 'prioridades-create',           component: PrioridadesCreateComponent },

    { path: 'sistemas-show',                component: SistemaShowComponent },
    { path: 'sistemas-update/:id',          component: SistemaUpdateComponent },
    { path: 'sistemas-create',              component: SistemaCreateComponent },

    { path: 'tarefa-status-show',           component: TarefaStatusShowComponent },
    { path: 'tarefa-status-update/:id',     component: TarefaStatusUpdateComponent },
    { path: 'tarefa-status-create',         component: TarefaStatusCreateComponent },

    { path: 'tarefa-tipo-show',             component: TarefaTipoShowComponent },
    { path: 'tarefa-tipo-update/:id',       component: TarefaTipoUpdateComponent },
    { path: 'tarefa-tipo-create',           component: TarefaTipoCreateComponent },

    { path: 'usuario-show',                 component: UsuarioShowComponent },
    { path: 'usuario-update/:id',           component: UsuarioUpdateComponent },
    { path: 'usuario-create',               component: UsuarioCreateComponent },

    { path: 'tarefa-show',                 component: TarefaShowComponent },
    { path: 'tarefa-update/:id',           component: TarefaUpdateComponent },
    { path: 'tarefa-create',               component: TarefaCreateComponent },
  
    { path: 'comentario-show',                 component: ComentarioShowComponent },
    { path: 'comentario-update/:id',           component: ComentarioUpdateComponent },
    { path: 'comentario-create',               component: ComentarioCreateComponent },

    { path: 'grupo-show',                 component: GrupoShowComponent },
    { path: 'grupo-update/:id',           component: GrupoUpdateComponent },
    { path: 'grupo-create',               component: GrupoCreateComponent },

    { path: 'projeto-show',                 component: ProjetoShowComponent },
    { path: 'projeto-update/:id',           component: ProjetoUpdateComponent },
    { path: 'projeto-create',               component: ProjetoCreateComponent },

    { path: 'projeto-usuario-show',                 component: ProjetoUsuarioShowComponent },
    { path: 'projeto-usuario-update/:id',           component: ProjetoUsuarioUpdateComponent },
    { path: 'projeto-usuario-create',               component: ProjetoUsuarioCreateComponent },

   
];
