import { AuthService } from './auth.service';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';
@Injectable({
  providedIn: 'root'
})

export class GlobalService {
  
  url = environment.url;
  constructor( 
    private http: HttpClient,
    private authService: AuthService
    ) { }

  // ----------------------Generico
  getAll(sufix){
    const token = this.authService.getToken();
    const headers = new HttpHeaders().set('x-access-token', `${token}`);
    return this.http.get(`${this.url}/${sufix}`, 
    {
      headers: headers
    }).pipe(map(data => data));
  }

  post(sufix, request){
    const token = this.authService.getToken();
    const headers = new HttpHeaders().set('x-access-token', `${token}`);
    return this.http.post(`${this.url}/${sufix}`, request, {
      headers: headers
    })
      .pipe(map(data => data));
  }

  delete(sufix, id){
    const token = this.authService.getToken();
    const headers = new HttpHeaders().set('x-access-token', `${token}`);
    return this.http.delete(`${this.url}/${sufix}/?id=${id}`, {
      headers: headers
    })
      .pipe(map(data => data));
  }

  edit(sufix, id, request){
    const token = this.authService.getToken();
    const headers = new HttpHeaders().set('x-access-token', `${token}`);
    return this.http.put(`${this.url}/${sufix}/?id=${id}`, request, {
      headers: headers
    })
    .pipe(map(data => data));
  }

  get(sufix, id){
    const token = this.authService.getToken();
    const headers = new HttpHeaders().set('x-access-token', `${token}`);
    return this.http.get(`${this.url}/${sufix}/?id=${id}`,{
      headers: headers
    })
    .pipe(map(data => data));
  }

}
