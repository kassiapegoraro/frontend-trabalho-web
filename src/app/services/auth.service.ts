
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, pipe } from 'rxjs';
import { environment } from 'environments/environment';
import { tap } from 'rxjs/internal/operators/tap';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private path = environment.url;
  
  constructor(
    private http: HttpClient
    ) { }

  me: any = null;

  login(credentials):Observable<{token:string}> {
    return this.http.post<{ token: string }>(`${this.path}/login`, credentials)
    .pipe(
      tap(response => {
        this.setToken(response.token);
      })
    );
  }

  setToken(token) {
    localStorage.setItem('token', token);
  }

  getToken() {  
    return localStorage.getItem('token');
  }

  // refreshToken() {
  //   return this.http.post(`${this.path}/refresh`, {})
  //   .pipe(map(data => data));
  // }

  logout(): Observable<any> {
    return this.http.post<{token:string}>(`${this.path}/logout`,{})
    .pipe(tap(() => {
      this.setToken(null);
    }));
  }
}